% Script for teaching the PRM example, found at:
% https://uk.mathworks.com/help/robotics/examples/path-planning-in-environments-of-difference-complexity.html
clear all
figure(1)
clf

%%%%%%%%%%%%%%%%%%%
% Import Example Maps for Planning a Path
%%%%%%%%%%%%%%%%%%%
load exampleMaps.mat

% Create binary occupancy map from  simpleMap with resolution 2 cells per
% metre
map = binaryOccupancyMap(simpleMap,2);
show(map)                               % Show the map

%%%%%%%%%%%%%%%%%%%
% Define Robot Dimensions and Inflate the Map
%%%%%%%%%%%%%%%%%%%

robotRadius = 0.2;      % Robot is a circle with radius 0.2 metres

mapInflated = copy(map);
inflate(mapInflated,robotRadius);
show(mapInflated)
title('Binary Occupancy Grid (Inflated)')

%%%%%%%%%%%%%%%%%%%
% Construct PRM and Set Parameters
%%%%%%%%%%%%%%%%%%%

% Create mobileRobotPRM object and define associated attributes
prm = mobileRobotPRM;
prm.Map = mapInflated;
prm.NumNodes = 50;

% Plot points
prm.ConnectionDistance = 0.1;
for i = 1:4
    disp(['Draw map ' num2str(i) ' of 4 (d = 0.1)'])
    update(prm)
    show(prm)
    title(['Probabilistic Roadmap for d = 0.1 (' num2str(i) ' of 4)'])
end

disp(' ')

% Plot points and links
prm.ConnectionDistance = 2;
for i = 1:4
    disp(['Draw map ' num2str(i) ' of 4 (d = 2)'])
    update(prm)
    show(prm)
    title(['Probabilistic Roadmap for d = 2 (' num2str(i) ' of 4)'])
end

disp(' ')

% Plot points and links
prm.NumNodes = 50;
prm.ConnectionDistance = 5;
for i = 1:4
    disp(['Draw map ' num2str(i) ' of 4 (d = 5)'])
    update(prm)
    show(prm)
    title(['Probabilistic Roadmap for d = 5 (' num2str(i) ' of 4)'])
end

disp(' ')

%%%%%%%%%%%%%%%%%%%
% Find a Feasible Path on the Constructed PRM
%%%%%%%%%%%%%%%%%%%
startLocation = [2 1];
endLocation = [12 10];

% Plot the start and end locations
hold on
scatter(startLocation(1), startLocation(2), 'filled', 'r')
scatter(endLocation(1), endLocation(2), 'filled', 'g')
hold off

% Find path using A* algorithm
% This is revealed in line 70 of the file at:
% MATLAB/R2019b/toolbox/shared/nav_rst/nav_rst_lib/+nav/+algs/+internal/PathFinder.m
path = findpath(prm, startLocation, endLocation)
disp(' ')

show(prm)
title('Path Through Probabilistic Roadmap')

%%%%%%%%%%%%%%%%%%%
% Use PRM for a large and Complicated Map
%%%%%%%%%%%%%%%%%%%

map2 = binaryOccupancyMap(complexMap,1);
show(map2)
title('Binary Occupancy Grid (Large)')

mapInflated2 = copy(map2);
inflate(mapInflated2, robotRadius);
show(mapInflated2)
title('Binary Occupancy Grid (Large and Inflated)')

%%%%%%%%%%%%%%%%%%%
% Find a Feasible Path on the Complex PRM
%%%%%%%%%%%%%%%%%%%

% Set start and finish locations
startLocation = [3 3];
endLocation = [45 35];
hold on
scatter(startLocation(1), startLocation(2), 'filled', 'r')
scatter(endLocation(1), endLocation(2), 'filled', 'g')
hold off

% Create PRM object referring to this graph
prm2 = mobileRobotPRM;
prm2.Map = mapInflated2;
prm2.NumNodes = 20;
prm2.ConnectionDistance = 15;

show(prm2)
hold on
scatter(startLocation(1), startLocation(2), 'filled', 'r')
scatter(endLocation(1), endLocation(2), 'filled', 'g')
hold off
title(['Probabilistic Roadmap (NumNodes = ' num2str(prm2.NumNodes) ')'])

path2 = findpath(prm2, startLocation, endLocation);

iteration_count = 0;
while isempty(path2)
    % Increment and display the iteration count
    iteration_count = iteration_count + 1;
    disp(['Performing iteration ' num2str(iteration_count) '...'])
    
    % No feasible path found yet, increase the number of nodes
    prm2.NumNodes = prm2.NumNodes + 10;
    
    % Use the |update| function to re-create the PRM roadmap with the changed
    % attribute
    update(prm2);
    
    % Draw the current PRM (without path)
    show(prm2)
    hold on
    scatter(startLocation(1), startLocation(2), 'filled', 'r')
    scatter(endLocation(1), endLocation(2), 'filled', 'g')
    hold off
    title(['Probabilistic Roadmap (NumNodes = ' num2str(prm2.NumNodes) ')'])
    
    % Search for a feasible path with the updated PRM
    path2 = findpath(prm2, startLocation, endLocation);
end

% Draw the PRM (with path)
disp('Suitable path found')
show(prm2)
title('Path Through Probabilistic Roadmap')

