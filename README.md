# GTA ACS337 Matlab

Work done while GTA'ing for the ACS337 Matlab assignment (replacement for e-pucks due to Coronavirus)

## Probabilistic Roadmap Lesson

The main thing in this repository is a lesson about Probabilistic Roadmaps. The idea is to go through the [example on the Mathworks website](https://uk.mathworks.com/help/robotics/examples/path-planning-in-environments-of-difference-complexity.html) in an online seminar. To make this easier, I've made the script `PRM_lesson.m`. This needs breakpoints on the following lines:

* 21
* 33
* 41
* 52
* 64
* 74
* 86
* 96
* 100
* 110
* 115
* 136
* 159

Load these with:

```matlab
Data = load('breakpoints.mat');
dbstop(Data.status);
```

Below is an outline of the lesson, including how to run the commands

* Import example maps
  * Start the script and it shows the simple, uninflated binary occupancy map
  * *We want to find a path that a robot can follow between two points on this graph*

* Define robot dimensions & inflate map (line 21)
  * The robot is a circle of radius 0.2 metres
  * Inflate the boundaries to reflect the space the robot can actually go through (we want the path the *centre* of the robot follows)
  * NEXT STEP
* Construct PRM & set parameters (line 33)
  * *How do you path plan from A to B?*
  * Clever algorithms find the shortest path between two nodes on a graph
  * We need the graph first
  * Currently the graph is literally any point on the graph, so searching through all of them would be inefficient
  * *We need to trim this down*
  * Our approach is to use a PRM: basically sampling random points
  * Next lines in script define this object in the code: 50 nodes
  * NEXT STEP (line 41)
  * Don't worry about connection distance yet, Just draw the nodes for now
  * NEXT STEP (line 41 a few times, note how the points change)
  * *Before the connection distance was very short: let's make it longer*
  * NEXT STEP (line 52 a few times, again note points change)
  * *Now make connection distance long enough to link all the nodes*
  * NEXT STEP (line 64 a few times)

* Find a path through these nodes (line 74)
  * We want to look for a path between two points
  * NEXT STEP (plots these points)
  * Now we can use our clever algorithm, in this case A*
  * NEXT STEP (draws the line)
* Use PRM for a larger map (line 96)
  * NEXT STEP (shows map)
  * Begin as before, by inflating the map
  * NEXT STEP (inflates map)
* Find path between these locations (line 110)
  * NEXT STEP (plots start and end points)
  * Define the new PRM object with a relatively large connection distance, but only a few nodes
  * NEXT STEP (plot this PRM)
* Start a loop, gradually increasing the number of nodes until  there's a path from A to B
  * NEXT STEP (line 136 two or three times)
  * *We're getting closer, but connection distance is 15 m. How would changing this affect the connections?*
  * NEXT STEP (line 136 until **suitable path found** is printed)
* Plot this path (line 159)
  * NEXT STEP